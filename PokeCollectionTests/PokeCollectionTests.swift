//
//  PokeCollectionTests.swift
//  PokeCollectionTests
//
//  Created by Sakdinan Sukkhasem on 2/5/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import XCTest
@testable import PokeCollection
class PokeCollectionTests: XCTestCase {
    
    func testInitializationSucceeds() {
        let zeroHardness = Pokemon.init(name: "Zero", photo: nil, hardness: 0)
        XCTAssertNotNil( zeroHardness )
        
        let positiveHardness = Pokemon.init(name: "Positive", photo: nil, hardness: 5)
        XCTAssertNotNil(positiveHardness)
    }
    
    func testInitializationFails(){
        
        let negativeHardness = Pokemon.init(name: "Negative", photo: nil, hardness: -1)
        XCTAssertNil(negativeHardness)
        
        // Rating exceeds maximum
        let largeHardness = Pokemon.init(name: "Large", photo: nil, hardness: 6)
        XCTAssertNil(largeHardness)
        
        // Empty String
        let emptyStringHardness = Pokemon.init(name: "", photo: nil, hardness: 0)
        XCTAssertNil(emptyStringHardness)
    }
}
