//
//  Hardness.swift
//  PokeCollection
//
//  Created by Sakdinan Sukkhasem on 2/4/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import UIKit

@IBDesignable class Hardness: UIStackView {
//MARK: Properties
    
    
    private var lvlButtons = [UIButton]()
    var hardness = 0 {
        didSet{
            updateButtonSelectionStates()
        }
    }
    @IBInspectable var starSize:CGSize = CGSize(width: 44 , height:44){
        didSet{
            setupButtons()
        }
    }
    @IBInspectable var starCount:Int =  5 {
        didSet{
            setupButtons()
        }
    }
    
//MARK: Initialize
   override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    
    required init(coder: NSCoder) {
       super.init(coder: coder)
        setupButtons()
    }
    //MARK: Button ACtion
    func hardnessButtontapped(button:UIButton){
        guard let index = lvlButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(lvlButtons)")
        }
        
        // Calculate the rating of the selected button
        let selectedLvl = index + 1
        
        if selectedLvl == hardness {
            // If the selected star represents the current rating, reset the rating to 0.
            hardness = 0
        } else {
            // Otherwise set the rating to the selected star
            hardness = selectedLvl
        }
    }
    private func updateButtonSelectionStates(){
        for ( index, button) in lvlButtons.enumerated(){
            button.isSelected = index < hardness
            // Set the hint string for the currently selected star
            let hintString: String?
            if hardness == index + 1 {
                hintString = "Tap to reset the rating to zero."
            } else {
                hintString = nil
            }
            
            // Calculate the value string
            let valueString : String
            switch (hardness) {
            case 0:
                valueString = "No rating set."
            case 1:
                valueString = "1 star set."
            
            default:
                valueString = "\(hardness) stars set."
            }
            // Assign the hint string and value string
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
        }
    }
    private func setupButtons(){
        
        for button in lvlButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        
        lvlButtons.removeAll()
        // Load Button Images
        let buddle = Bundle(for: type(of: self))
        let emptyStar = UIImage(named:"EmptyStar", in: buddle, compatibleWith: self.traitCollection)
        let filledStar = UIImage (named: "FilledStar", in: buddle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage (named: "HighlightedStar", in: buddle, compatibleWith: self.traitCollection)
        for index in 0..<5{
        let button = UIButton()
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
//        button.backgroundColor = UIColor.red
        //add Constraint
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: starSize.width).isActive = true
        button.widthAnchor.constraint(equalToConstant: starSize.height).isActive = false
        // Set the accessibility label
            button.accessibilityLabel = "set \(index + 1) Hardness"
        button.addTarget(self, action: #selector(Hardness.hardnessButtontapped(button:)), for: .touchUpInside)
        
        addArrangedSubview(button)
            lvlButtons.append(button)
        }
        updateButtonSelectionStates()
    }

}
