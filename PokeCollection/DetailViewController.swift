//
//  ViewController.swift
//  PokeCollection
//
//  Created by Sakdinan Sukkhasem on 2/4/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import UIKit
import os.log
class DetailViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: properties
    @IBOutlet weak var PokeNameLebel : UILabel!
    @IBOutlet weak var PokeTextField : UITextField!
    @IBOutlet weak var PokeImage: UIImageView!
    @IBOutlet weak var Hardness: Hardness!
    @IBOutlet weak var SaveButton: UIBarButtonItem!
    
    //MARK:

    var pokemon :Pokemon?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        PokeTextField.delegate = self
        PokeImage.isUserInteractionEnabled = true
        // Set up views if editing an existing Pokemon.
        if let pokemon = pokemon {
            navigationItem.title = pokemon.name
            PokeTextField.text = pokemon.name
            PokeImage.image = pokemon.photo
            Hardness.hardness = pokemon.hardness
        }
        updateSaveButtonState()
    }
    //MARK:- textFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        SaveButton.isEnabled = false
        PokeNameLebel.text = textField.text
        updateSaveButtonState()
        navigationItem.title = PokeTextField.text
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    //MARK: ImagePicker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        PokeImage.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
//MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === SaveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let name = PokeTextField.text ?? ""
        let photo = PokeImage.image
        let hardness = Hardness.hardness
        
        pokemon = Pokemon(name: name, photo: photo, hardness: hardness)
    }

//MARK:- Action Button
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {
        PokeTextField.resignFirstResponder()
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func setDefalutName(_sender:UIButton){
        PokeNameLebel.text = "Pokemon บีบมะนาวฆ่างู"
        
    }
    @IBAction func cancel(_sender:UIBarButtonItem){
        let isPresentingInAddPokemonMode = presentingViewController is UINavigationController
        if isPresentingInAddPokemonMode{
        dismiss(animated: true, completion: nil)
        }
        else if  let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
            
        }
        else {
            fatalError("The MealViewController is not inside a navigation controller.")
        }
    }
    
    //MARK: //MARK: Private Methods
    private func updateSaveButtonState(){
        // Disable the Save button if the text field is empty.
        let text = PokeTextField.text ?? ""
        SaveButton.isEnabled = !text.isEmpty
    }
    

    
    
  

}

