//
//  PokemonTableViewController.swift
//  PokeCollection
//
//  Created by Sakdinan Sukkhasem on 2/5/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import UIKit
import os.log
class PokemonTableViewController: UITableViewController {
    
    //MARK: Properties
    
    var Pokemons = [Pokemon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        if let savePokemon = loadpokemons() {
            Pokemons += savePokemon
        }
        else {
        loadSamplePokemons()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Pokemons.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellidentifier = "PokemonTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier, for: indexPath) as? PokemonTableViewCell else {
            fatalError("The dequeued cell is not an instance of PokemonTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let pokemon = Pokemons[indexPath.row]
        cell.pokeName.text = pokemon.name
        cell.pokeImage.image = pokemon.photo
        cell.hardness.hardness = pokemon.hardness
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
        Pokemons.remove(at: indexPath.row)
        savePokemons()
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        case "AddItem":
            os_log("Adding a new meal.", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let pokemonDetailViewController = segue.destination as? DetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
                
            }
            guard let selectedPokemonCell = sender as? PokemonTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            guard let indexpath = tableView.indexPath(for: selectedPokemonCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedPokemon = Pokemons[indexpath.row]
            pokemonDetailViewController.pokemon = selectedPokemon
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
            
        }
        
        
    }
    
    
    //MARK: Actions
    @IBAction func unwindTopokelist(sender : UIStoryboardSegue){
        if let sourceViewController = sender.source as? DetailViewController, let pokemon = sourceViewController.pokemon{
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                Pokemons[selectedIndexPath.row] = pokemon
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else{
                let newIndexPath = IndexPath(row: Pokemons.count, section: 0)
                Pokemons.append(pokemon)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            savePokemons()
        }
    }
    //MARK: Private Methods
    
    private func loadSamplePokemons() {
        let pokeImage1 = UIImage (named: "bulbasaur")
        let pokeImage2 = UIImage (named: "charmander")
        let pokeImage3 = UIImage (named: "squirtle")
        
        guard let poke1 = Pokemon(name: "bulbasaur", photo: pokeImage1, hardness: 2) else {
            fatalError("Unable to instantiate pokemon1")
        }
        guard let poke2 = Pokemon (name: "charmander", photo: pokeImage2, hardness: 3) else {
            fatalError("Unable to instantiate pokemon2")
        }
        guard let poke3 = Pokemon (name: "squirtle", photo: pokeImage3, hardness: 2) else {
            fatalError("Unable to instantiate pokemon3")
        }
        
        Pokemons += [poke1,poke2,poke3]
        
        
    }
    private func savePokemons(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(Pokemons, toFile: Pokemon.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
        
    }
    private func  loadpokemons()-> [Pokemon]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Pokemon.ArchiveURL.path) as? [Pokemon]
    }
    
}
