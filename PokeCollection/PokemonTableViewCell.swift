//
//  PokemonTableViewCell.swift
//  PokeCollection
//
//  Created by Sakdinan Sukkhasem on 2/5/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pokeName: UILabel!
    @IBOutlet weak var pokeImage: UIImageView!
    @IBOutlet weak var hardness: Hardness!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
