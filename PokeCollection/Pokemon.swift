//
//  Pokemon.swift
//  PokeCollection
//
//  Created by Sakdinan Sukkhasem on 2/5/2560 BE.
//  Copyright © 2560 Sakdinan Sukkhasem. All rights reserved.
//

import UIKit
import os.log

class Pokemon:NSObject, NSCoding {

    //MARK: properties
    var name: String
    var photo: UIImage?
    var hardness : Int
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("Pokemons")
    
    struct propertyKey {
        static let name = "name"
        static let photo = "photo"
        static let hardness = "hardness"
    }

    //MARK: Initialization
    init? (name: String, photo: UIImage? , hardness : Int) {
        
        if name.isEmpty || hardness < 0 {
            return nil
        }
        self.name = name
        self.photo = photo
        self.hardness = hardness
        
    }
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: propertyKey.name) as? String else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        // Because photo is an optional property of Meal, just use conditional cast.
        let photo = aDecoder.decodeObject(forKey: propertyKey.photo) as? UIImage
        
        let hardness = aDecoder.decodeInteger(forKey: propertyKey.hardness)
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, hardness: hardness)
        

    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: propertyKey.name)
        aCoder.encode(photo, forKey: propertyKey.photo )
        aCoder.encode(hardness, forKey: propertyKey.hardness)
    }
    
}
